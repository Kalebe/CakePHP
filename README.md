O objetivo do framework ou o porquê de sua criação; 
O CakePHP é um framework de desenvolvimento rápido para PHP, livre e de Código aberto. Nosso principal objetivo é permitir que você trabalhe de forma estruturada e rápida sem
perder a flexibilidade. O CakePHP tira a monotonia do desenvolvimento web. Nós fornecemos todas as ferramentas que você precisa para começar programando o que realmente deseja: a lógica específica da sua aplicação. Em vez de reinventar a roda a cada vez que se constrói um novo projeto, pegue uma cópia do CakePHP e comece com o interior de sua aplicação.

Requisitos básicos para poder utilizá-lo; 
Um banco de dados, e o cake php, conhecimentos de php.

Onde encontramos sua documentação e se existe em português; 
www.cakephpbrasil.com.br, não tem em portugues.

 Principais passos para instalação e configuração; 
Você precisa baixar e instalar o Composer, Se você tem cURL instalado, ele é tão fácil quanto executar o seguinte:
onda -s https://getcomposer.org/installer | php
Agora que você já baixou e instalou Compositor, digamos que você deseja criar uma nova aplicação CakePHP na pasta my_app_name, para isso basta executar o seguinte comando compositor:
A configuração é geralmente armazenado em ambos os arquivos INI PHP ou e carregado durante a inicialização do aplicativo. CakePHP vem com um arquivo de configuração por defeito, mas se necessário você pode adicionar arquivos de configuração adicionais e carregá-los em config / bootstrap.php . php composer.phar criar-projeto --prefer-dist cakephp my_app_name / app


 Principais vantagens do framework; 
Porque é um verdadeiro padrão de projeto e torna fácil a manutenção da sua aplicação, com pacotes modulares de rápido desenvolvimento. Elaborar tarefas divididas entre models, views e controllers faz com que sua aplicação fique leve e independente.

 Quais as desvantagens do framework encontradas durante o trabalho; Poucas classes de Helpers;  Exige mais habilidades do programador, caso queira se trabalhar com Design Patterns (Padrões de Projeto) ou modificar alguma estrutura do framework;  Atualização do framework requer testes no projeto;
